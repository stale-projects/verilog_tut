module reg_cell ( input wire clk, reset, write, read, 
		  inout data);

   parameter data_size = 8;

   reg [data_size-1:0] data_out;
   wire [data_size-1:0] data_in;
   wire [data_size-1:0] data;

   assign data <= (read) ? data_out : 'bz;
   assign data_in <= (write) ? data : data_out;
   
   always @(posedge clk)
     if (reset)
       data_out <= 'b0;
     else
       data_out <= data_in;

endmodule // register
