module TB (  );

   reg I;
   
   INVERTER INV1 (.I(I),.O(O));   
 
   initial begin
      I = 0;
      #10 I = 1;
      #10 $finish;
   end

   initial begin
      $shm_open("TB","w");
      $shm_probe(TB,"AC");
   end

endmodule // tb_inverter
