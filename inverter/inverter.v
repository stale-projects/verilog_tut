module INVERTER ( input wire I, output wire O );
   
   assign O = ~I;

endmodule // inverter