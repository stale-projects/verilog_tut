module tb_counter ();

   parameter counter_size = 4;
      
   reg clk = 0, rst, enable, en_sig, rst_sig;
   reg [counter_size-1:0] max_count, max_count_sig;
   wire top;
   wire [counter_size-1:0] count;
   
   always #5 clk <= ~clk;

   counter counter(.enable(en_sig),
		   .clk(clk),
		   .rst(rst_sig),
		   .count(count),
		   .top(top),
		   .max_count(max_count_sig));

   always @(posedge clk)
     begin
	max_count_sig <= max_count;
	en_sig <= enable;
	rst_sig <= rst;
     end
   
   initial
     begin
	rst = 1;
	#17 rst = 0;
     end

   initial
     begin
	enable <= 0;
	#12 enable <= 1;
	#50 enable <= 0;
	#24 enable <= 1;
	#70 enable <= 0;
	#42 enable <= 1;
	#50 $finish;
     end

   initial
     max_count <= 7;
   
   initial
     $monitor("%t : counter = %b (%0d), top = %b", $realtime, count, count, top);
   
endmodule // tb_counter