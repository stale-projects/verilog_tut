module DFF ( input wire D, CLK, EN, RST,  output reg Q);
   
   reg D_1;
   
   always @(posedge CLK)
     if (RST)
       begin
	  Q <= 0;
	  D_1 <= 0;
       end
     else
       if (EN)
	 begin
	    D_1 <= D;
	    Q   <= D_1;
	 end
   
endmodule // DFF
